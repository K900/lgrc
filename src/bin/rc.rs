use config::Config;
use crossterm::event::{read, Event, KeyCode, KeyEventKind};
use lgrc::{InputEvent, Key, LGRC};
use serde::Deserialize;
use tracing::{info, instrument};
use url::Url;

#[derive(Clone, Debug, Deserialize)]
struct RCConfig {
    url: String,
    key: Option<String>,
}

#[tokio::main]
#[instrument]
async fn main() -> eyre::Result<()> {
    tracing_subscriber::fmt::init();
    color_eyre::install()?;

    let settings: RCConfig = Config::builder()
        .add_source(config::File::with_name("config.toml"))
        .build()?
        .try_deserialize()?;

    let mut client = LGRC::new(Url::parse(&settings.url)?).await?;

    let key = client.handshake(settings.key.as_deref()).await?;
    info!("Auth key: {}", key);

    let mut input = client.get_remote_input().await?;

    info!("Use Up/Down/Left/Right for navigation");
    info!("Enter for OK, Esc for Back");
    info!("H for home menu, D for dashboard, M for side menu");
    info!("+/- to control volume");

    loop {
        let Event::Key(k) = read()? else {
            continue;
        };

        if k.kind != KeyEventKind::Release {
            continue;
        }

        let key = match k.code {
            KeyCode::Up => Key::Up,
            KeyCode::Down => Key::Down,
            KeyCode::Left => Key::Left,
            KeyCode::Right => Key::Right,
            KeyCode::Enter => Key::OK,
            KeyCode::Esc => Key::Back,
            KeyCode::Char('h') => Key::Home,
            KeyCode::Char('d') => Key::Dash,
            KeyCode::Char('m') => Key::Menu,
            KeyCode::Char('=') => Key::VolumeUp,
            KeyCode::Char('-') => Key::VolumeDown,
            _ => continue,
        };

        input.send(InputEvent::Key(key)).await?;
    }
}
