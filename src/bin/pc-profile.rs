use std::process::ExitStatus;

use config::Config;
use futures::StreamExt;
use serde::Deserialize;
use tokio::process::Command;
use tracing::{info, instrument, warn};
use url::Url;

use lgrc::messages::{ActiveAppSubscription, GetExternalInputListCommand};
use lgrc::LGRC;

#[derive(Clone, Debug, Deserialize)]
struct CommandConfig {
    command: String,

    #[serde(default)]
    args: Vec<String>,
}

impl CommandConfig {
    #[instrument]
    async fn run(&self) -> eyre::Result<ExitStatus> {
        info!("Running commmand...");
        let res = Command::new(&self.command)
            .args(&self.args)
            .spawn()?
            .wait()
            .await?;
        info!("Done!");
        Ok(res)
    }
}

#[derive(Clone, Debug, Deserialize)]
struct PCProfileConfig {
    url: String,
    key: Option<String>,
    pc_output: String,

    pc_command: CommandConfig,
    not_pc_command: CommandConfig,
}

#[derive(PartialEq, Eq)]
enum OutputType {
    Unknown,
    PC,
    NotPC,
}

struct OutputSwitcher<'a> {
    current: OutputType,
    pc_command: &'a CommandConfig,
    not_pc_command: &'a CommandConfig,
}

impl<'a> OutputSwitcher<'a> {
    fn from_config(conf: &'a PCProfileConfig) -> Self {
        Self {
            current: OutputType::Unknown,
            pc_command: &conf.pc_command,
            not_pc_command: &conf.not_pc_command,
        }
    }

    async fn set(&mut self, output: OutputType) -> eyre::Result<()> {
        if self.current != output {
            self.current = output;
            match self.current {
                OutputType::Unknown => panic!("This should never happen!"),
                OutputType::PC => self.pc_command.run().await?,
                OutputType::NotPC => self.not_pc_command.run().await?,
            };
        }

        Ok(())
    }
}

#[tokio::main]
#[instrument]
async fn main() -> eyre::Result<()> {
    tracing_subscriber::fmt::init();
    color_eyre::install()?;

    let settings: PCProfileConfig = Config::builder()
        .add_source(config::File::with_name("config.toml"))
        .build()?
        .try_deserialize()?;

    let mut switcher = OutputSwitcher::from_config(&settings);

    loop {
        let mut client = LGRC::new(Url::parse(&settings.url)?).await?;

        let key = client.handshake(settings.key.as_deref()).await?;
        info!("Auth key: {}", key);

        let inputs = client.command(GetExternalInputListCommand {}).await?;

        let pc_app_id = inputs
            .devices
            .into_iter()
            .find(|x| x.label == settings.pc_output)
            .ok_or(eyre::eyre!("Failed to find PC output!"))?
            .app_id;

        info!("Ready!");

        let mut rx = client.subscribe::<ActiveAppSubscription>().await?;

        while let Some(value) = rx.next().await {
            let output_type = if value.app_id == pc_app_id {
                OutputType::PC
            } else {
                OutputType::NotPC
            };
            switcher.set(output_type).await?;
        }
    }
}
