use std::{
    fmt::{Debug, Display},
    sync::Arc,
};

use dashmap::DashMap;
use futures::{
    stream::{SplitSink, SplitStream},
    SinkExt, Stream, StreamExt,
};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::Value;
use tokio::{
    net::TcpStream,
    sync::mpsc::{channel, Receiver, Sender},
};
use tokio_stream::wrappers::ReceiverStream;
use tokio_tungstenite::{connect_async, tungstenite::Message, MaybeTlsStream, WebSocketStream};
use tracing::{trace, warn};
use url::Url;
use uuid::Uuid;

pub mod messages;

use messages::{AsRequest, AsSubscription, GetPointerInputSocketCommand};

#[derive(Debug, Serialize)]
pub struct RequestMessage {
    id: Uuid,
    r#type: String,
    uri: Option<String>,
    payload: Option<Value>,
}

impl RequestMessage {
    fn uuid(&self) -> Uuid {
        self.id
    }

    pub(crate) fn from_command<T>(command: T) -> Self
    where
        T: AsRequest,
    {
        Self {
            id: Uuid::new_v4(),
            r#type: "request".into(),
            uri: Some(T::URL.into()),
            payload: command.payload(),
        }
    }

    fn from_subscription<T>() -> Self
    where
        T: AsSubscription,
    {
        Self {
            id: Uuid::new_v4(),
            r#type: "subscribe".into(),
            uri: Some(T::URL.into()),
            payload: None,
        }
    }

    fn register(key: Option<&str>) -> Self {
        let mut payload: Value = serde_json::from_str(include_str!("./handshake.json"))
            .expect("Could not parse handshake payload");

        if let Some(key) = key {
            payload["client-key"] = Value::from(key);
        }

        Self {
            id: Uuid::new_v4(),
            r#type: "register".into(),
            uri: None,
            payload: Some(payload),
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct ResponseMessage {
    pub id: Uuid,
    pub r#type: String,
    pub payload: Option<Value>,
}

type WS = WebSocketStream<MaybeTlsStream<TcpStream>>;
type WSSink = SplitSink<WS, Message>;
type WSStream = SplitStream<WS>;

pub struct LGRC {
    tx: Sender<RequestMessage>,
    subscriptions: Arc<DashMap<Uuid, Sender<ResponseMessage>>>,
}

impl LGRC {
    #[tracing::instrument(skip(tx_recv, tx))]
    async fn pump_tx(mut tx_recv: Receiver<RequestMessage>, mut tx: WSSink) -> eyre::Result<()> {
        while let Some(message) = tx_recv.recv().await {
            let message_json = serde_json::to_string(&message)?;
            trace!("Sending event: {}", message_json);

            tx.send(Message::text(message_json)).await?;
        }

        Ok(())
    }

    #[tracing::instrument(skip(rx, subscriptions))]
    async fn pump_rx(
        mut rx: WSStream,
        subscriptions: Arc<DashMap<Uuid, Sender<ResponseMessage>>>,
    ) -> eyre::Result<()> {
        while let Some(message) = rx.next().await {
            match message {
                Ok(Message::Text(text)) => {
                    trace!("Received event: {}", text);
                    match serde_json::from_str::<ResponseMessage>(&text) {
                        Ok(msg) => {
                            match subscriptions.get_mut(&msg.id) {
                                Some(sender) => {
                                    if let Err(e) = sender.send(msg).await {
                                        warn!("Failed to send message to consumer: {:?}", e);
                                    };
                                }
                                None => warn!("Unmatched message: {}", msg.id),
                            };
                        }
                        Err(e) => warn!("Failed to parse message: {:?}", e),
                    }
                }
                Ok(Message::Ping(_) | Message::Pong(_)) => {}
                Ok(Message::Close(_)) => subscriptions.clear(),
                Ok(m) => warn!("Got a non-text message: {:?}", m),
                Err(e) => warn!("Got an error: {:?}", e),
            }
        }

        Ok(())
    }

    #[tracing::instrument]
    pub async fn new(url: Url) -> eyre::Result<Self> {
        let (stream, _) = connect_async(url).await?;
        let (tx, rx) = stream.split();

        let subscriptions = Arc::new(DashMap::new());

        let (tx_send, tx_recv) = channel::<RequestMessage>(10);

        tokio::spawn(Self::pump_tx(tx_recv, tx));
        tokio::spawn(Self::pump_rx(rx, subscriptions.clone()));

        let s = Self {
            tx: tx_send,
            subscriptions,
        };

        Ok(s)
    }

    #[tracing::instrument(skip(self))]
    pub async fn handshake(&mut self, key: Option<&str>) -> eyre::Result<String> {
        let (uuid, mut rx) = self.request(RequestMessage::register(key)).await?;

        let key = loop {
            let response = match rx.recv().await {
                Some(m) => m,
                None => eyre::bail!("Stream closed before handshake response!"),
            };

            if let ResponseMessage {
                id: _,
                r#type: t,
                payload: Some(p),
            } = response
            {
                if t == "registered" {
                    let key_json = match p.get("client-key") {
                        Some(k) => k,
                        None => eyre::bail!("No client-key in response: {:?}!", p),
                    };

                    let key = match key_json.as_str() {
                        Some(k) => k.into(),
                        None => eyre::bail!("client-key not a string: {:?}!", key_json),
                    };

                    break key;
                }
            }
        };

        self.subscriptions.remove(&uuid);

        Ok(key)
    }

    #[tracing::instrument(skip(self))]
    async fn send(&mut self, message: RequestMessage) -> eyre::Result<()> {
        Ok(self.tx.send(message).await?)
    }

    #[tracing::instrument(skip(self))]
    async fn request(
        &mut self,
        message: impl Into<RequestMessage> + Debug,
    ) -> eyre::Result<(Uuid, Receiver<ResponseMessage>)> {
        let request = message.into();
        let uuid = request.uuid();

        let (tx, rx) = channel(10);
        self.subscriptions.insert(uuid, tx);
        self.send(request).await?;

        Ok((uuid, rx))
    }

    #[tracing::instrument(skip(self))]
    pub async fn command<R>(&mut self, message: R) -> eyre::Result<R::Response>
    where
        R: AsRequest + Debug,
        R::Response: DeserializeOwned,
    {
        let (uuid, mut rx) = self.request(RequestMessage::from_command(message)).await?;

        if let Some(m) = rx.recv().await {
            self.subscriptions.remove(&uuid);
            return Ok(serde_json::from_value(m.payload.unwrap_or(Value::Null))?);
        }

        eyre::bail!("Stream closed!")
    }

    #[tracing::instrument(skip(self))]
    pub async fn subscribe<S>(&mut self) -> eyre::Result<impl Stream<Item = S::Message>>
    where
        S: AsSubscription,
        S::Message: DeserializeOwned,
    {
        let (_, rx) = self
            .request(RequestMessage::from_subscription::<S>())
            .await?;

        let stream = ReceiverStream::new(rx).map(|s| {
            serde_json::from_value(s.payload.unwrap_or(Value::Null))
                .expect("Failed to deserialize response")
        });

        Ok(stream)
    }

    #[tracing::instrument(skip(rx))]
    async fn pump_rx_input(mut rx: WSStream) -> eyre::Result<()> {
        while let Some(message) = rx.next().await {
            match message {
                Ok(Message::Text(text)) => {
                    trace!("Received event: {}", text);
                }
                Ok(Message::Ping(_) | Message::Pong(_)) => {}
                Ok(m) => warn!("Got a non-text message: {:?}", m),
                Err(e) => warn!("Got an error: {:?}", e),
            }
        }

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    pub async fn get_remote_input(&mut self) -> eyre::Result<RemoteInput> {
        let response = self.command(GetPointerInputSocketCommand {}).await?;
        let (stream, _) = connect_async(response.socket_path).await?;
        let (tx, rx) = stream.split();

        tokio::spawn(Self::pump_rx_input(rx));

        Ok(RemoteInput { sink: tx })
    }
}

#[derive(Debug)]
pub enum Key {
    Up,
    Down,
    Left,
    Right,
    OK,
    Back,
    Home,
    Dash,
    Menu,
    VolumeUp,
    VolumeDown,
}

impl Display for Key {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Key::*;
        f.write_str(match self {
            Up => "UP",
            Down => "DOWN",
            Left => "LEFT",
            Right => "RIGHT",
            OK => "ENTER",
            Back => "BACK",
            Home => "HOME",
            Dash => "DASH",
            Menu => "MENU",
            VolumeUp => "VOLUMEUP",
            VolumeDown => "VOLUMEDOWN",
        })
    }
}

#[derive(Debug)]
pub enum InputEvent {
    Key(Key),
    Move { dx: usize, dy: usize },
    Scroll { dx: usize, dy: usize },
    Click,
}

impl Display for InputEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use InputEvent::*;
        match self {
            Key(k) => write!(f, "type:button\nname:{}", k),
            Move { dx, dy } => write!(f, "type:move\ndx:{}\ndy:{}", dx, dy),
            Scroll { dx, dy } => write!(f, "type:scroll\ndx:{}\ndy:{}", dx, dy),
            Click => f.write_str("type:click"),
        }
    }
}

pub struct RemoteInput {
    sink: WSSink,
}

impl RemoteInput {
    #[tracing::instrument(skip(self))]
    pub async fn send(&mut self, event: InputEvent) -> eyre::Result<()> {
        self.sink
            .send(Message::Text(format!("{}\n\n", event)))
            .await?;
        Ok(())
    }
}
