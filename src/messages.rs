use serde::Deserialize;

pub trait AsRequest {
    const URL: &'static str;
    type Response;

    fn payload(&self) -> Option<serde_json::Value> {
        None
    }
}

pub trait AsSubscription {
    const URL: &'static str;
    type Message;
}

#[derive(Debug)]
pub struct GetExternalInputListCommand {}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Device {
    pub app_id: String,
    pub connected: bool,
    pub favorite: bool,
    pub force_icon: bool,
    pub hdmi_plug_in: bool,
    pub icon: String,
    pub id: String,
    pub label: String,
    // more
}

#[derive(Debug, Deserialize)]
pub struct GetExternalInputListResponse {
    pub devices: Vec<Device>,
}

impl AsRequest for GetExternalInputListCommand {
    const URL: &'static str = "ssap://tv/getExternalInputList";

    type Response = GetExternalInputListResponse;
}

pub struct ActiveAppSubscription {}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ActiveAppMessage {
    pub app_id: String,
}

impl AsSubscription for ActiveAppSubscription {
    const URL: &'static str = "ssap://com.webos.applicationManager/getForegroundAppInfo";

    type Message = ActiveAppMessage;
}

#[derive(Debug)]
pub struct GetPointerInputSocketCommand {}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GetPointerInputSocketResponse {
    pub socket_path: String,
}

impl AsRequest for GetPointerInputSocketCommand {
    const URL: &'static str = "ssap://com.webos.service.networkinput/getPointerInputSocket";

    type Response = GetPointerInputSocketResponse;
}
