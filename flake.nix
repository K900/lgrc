{
  description = "Here be heinous crimes";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    cross = {
      url = "gitlab:K900/rust-cross-windows";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    cross,
    pre-commit-hooks,
  }: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in {
    packages.x86_64-linux = rec {
      lgrc = cross.lib.mkNativePackage {
        src = ./.;
      };

      lgrcWindows = cross.lib.mkCrossPackage {
        src = ./.;
      };

      default = lgrc;
    };

    devShells.x86_64-linux.default = cross.lib.mkCrossShell {
      inputsFrom = with self.packages.x86_64-linux; [lgrc lgrcWindows];
      buildInputs = [pkgs.cargo-outdated pkgs.rustfmt];

      inherit (self.checks.x86_64-linux.pre-commit-check) shellHook;
    };

    checks.x86_64-linux.pre-commit-check = pre-commit-hooks.lib.x86_64-linux.run {
      src = ./.;
      hooks = {
        alejandra.enable = true;
        deadnix.enable = true;
        statix.enable = true;

        rustfmt.enable = true;
        clippy.enable = true;
        cargo-check.enable = true;
      };
      tools = {
        cargo = cross.lib.crossToolchain;
        clippy = cross.lib.crossToolchain;
      };
    };
  };
}
